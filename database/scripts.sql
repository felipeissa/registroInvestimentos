create schema controle_investimentos;

use controle_investimentos;

CREATE TABLE TIPOS_APLICACAO(
  id integer primary key AUTO_INCREMENT,
  nome VarChar(30) not null,
  imposto_de_renda bit not null);
insert into TIPOS_APLICACAO (nome, imposto_de_renda) values ('LCI', 0);
insert into TIPOS_APLICACAO (nome, imposto_de_renda) values ('LCA', 0);
insert into TIPOS_APLICACAO (nome, imposto_de_renda) values ('CDB', 1);
insert into TIPOS_APLICACAO (nome, imposto_de_renda) values ('LC', 1);
insert into TIPOS_APLICACAO (nome, imposto_de_renda) values ('Tesouro Direto', 1);

CREATE TABLE INDEXADOR (
  id integer primary key AUTO_INCREMENT,
  nome Varchar(30) not null);

insert into INDEXADOR (nome) values ('Selic');
insert into INDEXADOR (nome) values ('CDI');
insert into INDEXADOR (nome) values ('Pre fixado');

CREATE TABLE USERS (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    username VarChar(30),
    fullname VarChar(80),
    email Varchar(60),
    salt Varchar(30),
    fb_id VarChar(30),
    fb_token VarChar(300),
    password VarChar(128),
    data_cadastro bigint not null
);
CREATE UNIQUE INDEX UK_USERS_USERNAME on USERS(username);

CREATE TABLE CORRETORA (
  id integer primary key AUTO_INCREMENT,
  nome VarChar(40) not null,
  cnpj VarChar(40) not null,
  data_cadastro bigint not null,
  id_usuario integer not null,
FOREIGN KEY(id_usuario) REFERENCES USERS(id));
CREATE UNIQUE INDEX UK_CORRETORA_NOME on CORRETORA(nome, id_usuario);

CREATE TABLE ACAO (
  id integer primary key AUTO_INCREMENT,
  sigla VarChar(6) not null,
  nome VarChar(40) not null,
  data_cadastro bigint not null,
  data_ultima_compra bigint null,
  id_usuario integer not null,
FOREIGN KEY(id_usuario) REFERENCES USERS(id));
CREATE UNIQUE INDEX UK_ACAO_SIGLA on ACAO(sigla, id_usuario);

CREATE TABLE APLICACAO_RENDA_FIXA (
   id integer primary key AUTO_INCREMENT,
   data bigint not null,
   id_tipo_aplicacao integer not null,
   id_corretora integer not null,
   vencimento bigint not null,
   taxa float not null,
   id_indexador integer not null,
   valor float not null,
   id_usuario int not null,
   FOREIGN KEY(id_tipo_aplicacao) REFERENCES TIPOS_APLICACAO(id),
   FOREIGN KEY(id_corretora) REFERENCES CORRETORA(id),
   FOREIGN KEY(id_indexador) REFERENCES INDEXADOR(id),
   FOREIGN KEY(id_usuario) REFERENCES USERS(id));

CREATE TABLE NOTA_FISCAL_RENDA_VARIAVEL (
  id integer primary key AUTO_INCREMENT,
  data bigint not null,
  id_corretora integer not null,
  nota_fiscal integer not null,
  liquidacao float not null,
  emolumentos float not null,
  corretagem float not null,
  iss float not null,
  id_usuario integer not null,
  FOREIGN KEY(id_corretora) REFERENCES CORRETORA(id),
  FOREIGN KEY(id_usuario) REFERENCES USERS(id));
CREATE UNIQUE INDEX UK_NFRV_NF_CORRETORA on NOTA_FISCAL_RENDA_VARIAVEL(nota_fiscal, id_corretora);

CREATE TABLE NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO (
  id integer primary key  AUTO_INCREMENT,
  id_nota_fiscal integer,
  id_acao integer,
  quantidade integer,
  valor float,
  corretagem float,
  venda bit,
  FOREIGN KEY(id_nota_fiscal) REFERENCES NOTA_FISCAL_RENDA_VARIAVEL(id),
  FOREIGN KEY(id_acao) REFERENCES ACAO(id));

CREATE TABLE CONFIGURACAO_SISTEMA(
  code varchar(40) primary key,
  value varchar(100) not null
);

insert into CONFIGURACAO_SISTEMA values ('fbClientId', 'CLIENT_ID');
insert into CONFIGURACAO_SISTEMA values ('fbClientSecret', 'CLIENT_SECRET');
insert into CONFIGURACAO_SISTEMA values ('fbCallbackUrl', 'http://localhost:8080/auth/facebook/callback');
