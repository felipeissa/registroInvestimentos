angular.module("financeiro").config(function ($routeProvider){

	$routeProvider.when("/listaAcao", {
		templateUrl: "view/listaAcao.html",
		controller: "listaAcaoCtrl",
		resolve: {
			 acoes: function(rendaVariavelAPI){
			 	return rendaVariavelAPI.getAcoes();
			 }
		}
	});

	$routeProvider.when("/listaCorretora", {
		templateUrl: "view/listaCorretora.html",
		controller: "listaCorretoraCtrl",
		resolve: {
			 corretoras: function(rendaFixaAPI){
			 	return rendaFixaAPI.getCorretoras();
			 }
		}
	});

	$routeProvider.when("/aplicacaoRendaFixa", {
		templateUrl: "view/aplicacaoRendaFixa.html",
		controller: "aplicacaoRendaFixaCtrl",
		resolve: {
			 tiposAplicacao: function(rendaFixaAPI){
			 	return rendaFixaAPI.getTiposAplicacao();
			 },
			 corretoras: function(rendaFixaAPI){
			 	return rendaFixaAPI.getCorretoras();
			 },
			 indexadores: function(rendaFixaAPI){
			 	return rendaFixaAPI.getIndexadores();
			 }
		}
	});

	$routeProvider.when("/aplicacaoRendaVariavel", {
		templateUrl: "view/aplicacaoRendaVariavel.html",
		controller: "aplicacaoRendaVariavelCtrl",
		resolve: {
			 corretoras: function(rendaFixaAPI){
			 	return rendaFixaAPI.getCorretoras();
			 },
			 acoes: function(rendaVariavelAPI)
			 {
			 	return rendaVariavelAPI.getAcoes();
			 }
		}
	});

	$routeProvider.when("/minhaRendaFixa", {
		templateUrl: "view/minhaRendaFixa.html",
		controller: "minhaRendaFixaCtrl",
		resolve: {
			 aplicacoes: function(rendaFixaAPI){
			 	return rendaFixaAPI.getAplicacoes()
			 }
		}
	});

	$routeProvider.when("/simulacaoRendaFixa", {
		templateUrl: "view/simulacaoRendaFixa.html",
		controller: "simulacaoRendaFixaCtrl",
		resolve: {
			 tiposAplicacao: function(rendaFixaAPI){
			 	return rendaFixaAPI.getTiposAplicacao();
			 },
			 indexadores: function(rendaFixaAPI){
			 	return rendaFixaAPI.getIndexadores();
			 }
		}
	});

	$routeProvider.when("/minhaRendaVariavel", {
		templateUrl: "view/minhaRendaVariavel.html",
		controller: "minhaRendaVariavelCtrl",
		resolve: {
			 acoes: function(rendaVariavelAPI){
			   	return rendaVariavelAPI.getConsolidadoAcoes()
			 }
		}
	});

	$routeProvider.when("/consolidado", {
		templateUrl: "view/consolidado.html",
		controller: "consolidadoCtrl",
		resolve: {
			aplicacoes: function(rendaFixaAPI){
			  return rendaFixaAPI.getAplicacoes()
		  },
			acoes: function(rendaVariavelAPI){
				return rendaVariavelAPI.getConsolidadoAcoes()
			}
		}
	});

	$routeProvider.when("/irAcoes", {
		templateUrl: "view/irAcoes.html",
		controller: "irAcoesCtrl",
	});

	$routeProvider.otherwise({redirectTo: "/consolidado"});
});
