angular.module("financeiro").directive("uiDouble", function($filter){
	return {
		require: "ngModel",
		link: function(scope, element, attrs, ctrl){
			var _formatValue = function(number){
				if(!number)
				{
					return number;
				}
				//remove non numeric characters
				number = number.replace(/[^0-9]/g, "");

				//remove leading 0
				number = number.replace(/^0+/, "");
				
				if(number.length == 0){
					return "0.00";
				}

				while(number.length < 3){
					number = "0" + number;
				}

				number = number.substring(0,number.length -2) + "." + number.substring(number.length -2);
				
				/*
				var count = 6;
				while (count < number.length)
				{
					number = number.substring (0, number.length-count) + "." + number.substring(number.length-count);
					count+=4;
				}*/

				return number;
			}

			element.bind("keyup", function()
			{
				ctrl.$setViewValue(_formatValue(ctrl.$viewValue))	;
				ctrl.$render();
			});
		}
	}
});