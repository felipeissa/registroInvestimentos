angular.module("financeiro").factory("rendaVariavelAPI", function($http, config){
	var _getAcoes = function ()	{
		return $http.get(config.baseUrl + "/acao");
	}

	var _saveAcao = function (dados)	{
		return $http.post(config.baseUrl + "/acao", dados);
	}

	var _saveNotaFiscal = function (dados)	{
		return $http.post(config.baseUrl + "/notaFiscalRendaVariavel", dados);
	}

	var _getConsolidadoAcoes = function ()	{
		return $http.get(config.baseUrl + "/consolidadoAcoes");
	}

	var _getConsolidadoAcao = function (idAcao)	{
		return $http.get(config.baseUrl + "/consolidadoAcao/" + idAcao);
	}

	var _getHistoricoAcoesUntilDate = function (date)	{
		return $http.get(config.baseUrl + "/getHistoricoUntilDate/" + date);
	}

	return {
		getAcoes: _getAcoes,
		saveAcao: _saveAcao,
		saveNotaFiscal: _saveNotaFiscal,
		getConsolidadoAcoes: _getConsolidadoAcoes,
		getConsolidadoAcao: _getConsolidadoAcao,
		getHistoricoAcoesUntilDate : _getHistoricoAcoesUntilDate
	};
});
