angular.module("financeiro").factory("rendaFixaAPI", function($http, config){
	var _getTiposAplicacao = function ()	{
		return $http.get(config.baseUrl + "/tipoAplicacao");
	}

	var _getCorretoras = function ()	{
		return $http.get(config.baseUrl + "/corretora");
	}

	var _saveAplicacao = function (aplicacao)	{
		return $http.post(config.baseUrl + "/aplicacaoRendaFixa", aplicacao);
	}

	var _getAplicacoes = function ()	{
		return $http.get(config.baseUrl + "/aplicacaoRendaFixa");
	}

	var _getIndexadores = function ()	{
		return $http.get(config.baseUrl + "/indexador");
	}

	var _getCorretoraById = function(id){
		return $http.get(config.baseUrl + "/corretora/" + id);
	}

	var _getIndexadorById = function(id){
		return $http.get(config.baseUrl + "/indexador/" + id);
	}

	var _getTipoAplicacaoById = function(id){
		return $http.get(config.baseUrl + "/tipoAplicacao/" + id);
	}

	var _saveCorretora = function (corretora)	{
		return $http.post(config.baseUrl + "/corretora", corretora);
	}

	return {
		getTiposAplicacao: _getTiposAplicacao,
		getCorretoras: _getCorretoras,
		saveCorretora: _saveCorretora,
		saveAplicacao: _saveAplicacao,
		getAplicacoes: _getAplicacoes,
		getIndexadores: _getIndexadores,
		getCorretoraById: _getCorretoraById,
		getTipoAplicacaoById: _getTipoAplicacaoById,
		getIndexadorById: _getIndexadorById
	};
});
