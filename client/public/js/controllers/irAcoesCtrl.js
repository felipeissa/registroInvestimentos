angular.module("financeiro").controller("irAcoesCtrl", function($scope, rendaVariavelAPI){
	$scope.app = "Imposto de renda ações";

	$scope.anosDisponiveis = ['2016', '2015', '2014', '2013']

	$scope.gerarImpostoRenda = function(anoBase)
	{
		var finalAnoBase = new Date("01/01/" + (parseInt(anoBase) + 1) + " GMT")
		rendaVariavelAPI.getHistoricoAcoesUntilDate(finalAnoBase.getTime()).success(
			function(data)
			{
				impostoRenda = [];

				var acaoAtual = {};
				data.dados.forEach(function(linha){
					if(acaoAtual.sigla != linha.sigla)
					{
						acaoAtual = {
							sigla: linha.sigla,
							nome: linha.nomeAcao,
							quantidade : 0,
							valorTotal : 0
						};

						impostoRenda.push(acaoAtual);
					}

					var quantidade = parseInt(linha.quantidade);

					if(linha.venda)
					{
						//usa preco medio na venda
						acaoAtual.valorTotal -= acaoAtual.valorTotal > 0? (acaoAtual.valorTotal/ acaoAtual.quantidade * quantidade) : 0;
						acaoAtual.quantidade -= quantidade;
					}
					else {
						var valorOperacao = (parseFloat(linha.valor) * quantidade) + parseFloat(linha.corretagem);
						acaoAtual.quantidade+= quantidade;
						acaoAtual.valorTotal+= valorOperacao;
					}

					$scope.impostoRenda = impostoRenda;
				})
			}
		).error(function()
	{
		console.log("erro");
	});
	}
});
