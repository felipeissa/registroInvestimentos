angular.module("financeiro").controller("simulacaoRendaFixaCtrl", function($scope, tiposAplicacao, indexadores, $modal){
	$scope.app = "Simulação aplicação de renda fixa";

	$scope.tiposAplicacao = tiposAplicacao.data.dados;
	$scope.indexadores = indexadores.data.dados;

	var simulacao = {
	 	aplicacoes : [novaAplicacao(), novaAplicacao()]
	};

	function novaAplicacao()
	{
		return {taxaIntermediacao:0}
	}

	$scope.simulacao = simulacao;

	$scope.adicionarLinha = function()
	{
		simulacao.aplicacoes.push(novaAplicacao());
	}

	$scope.hasLinhaSelecionada = function(linhas)
	{
		return linhas.some(function (linha){
			return linha.selecionado
		});
	}

	$scope.apagarLinha = function(linhas)
	{
		simulacao.aplicacoes = linhas.filter(function (linha){
			if(!linha.selecionado) return linha;
		});
	};

	$scope.simular = function(simulacao)
	{
		var modalInstance = $modal.open({
	        animation: false,
	        templateUrl: "view/resultadoSimulacaoRendaFixa.html",
	        controller: "resultadoSimulacaoRendaFixaCtrl",
					size: 'lg',
	        resolve: {
  	            simulacao: function () {
	                return simulacao;
	            }
	      	}
	    });

	    modalInstance.result.then(function (confirmado) {
	      	//does nothing
	    }, function () {
				//does nothing
	    });
	}

});
