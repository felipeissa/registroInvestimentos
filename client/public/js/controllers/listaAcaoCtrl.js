angular.module("financeiro").controller("listaAcaoCtrl", function($scope, $modal, acoes){
	$scope.app = "Ações cadastradas";

	$scope.acoes = acoes.data.dados;

	$scope.cadastrarAcao = function()
	{
		var modalInstance = $modal.open({
	        animation: false,
	        templateUrl: "view/cadastroAcao.html",
	        controller: "cadastroAcaoCtrl",
					size: 'lg'
	    });

			modalInstance.result.then(function (result) {
				if(result){
					$scope.acoes.push(result);
				}
			});
	};

	$scope.ordenarPor = function(campo)
	{
		$scope.criterioDeOrdenacao = campo;
		$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	}
});
