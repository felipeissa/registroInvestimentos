angular.module("financeiro").controller("aplicacaoRendaFixaCtrl", function($scope, tiposAplicacao, corretoras, rendaFixaAPI, indexadores, $location, toastr){
	$scope.app = "Nova aplicação de renda fixa";
	$scope.tiposAplicacao = tiposAplicacao.data.dados;
	$scope.corretoras = corretoras.data.dados;
	$scope.indexadores = indexadores.data.dados;

	$scope.adicionarAplicacao = function(aplicacao) {
		rendaFixaAPI.saveAplicacao(aplicacao).success(function(data){
			if(data.status){
				toastr.success('', 'Aplicação cadastrada com sucesso!', {timeOut: 2000});
				$location.path("/minhaRendaFixa");
			}else{
				toastr.error('', 'Falha ao inserir registro no sistema!', {timeOut: 2000});
				console.log(data.mensagem);
			}

		}).error(function(data){
			console.log(data);
			$scope.error = "Não foi possivel salvar os dados.";
		});;
	};

	$scope.aplicacao = {
	 	data : new Date().getTime()
	}
});
