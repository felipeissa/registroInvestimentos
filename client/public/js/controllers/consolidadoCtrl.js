angular.module("financeiro").controller("consolidadoCtrl", function($scope, $modal, rendaFixaAPI, aplicacoes, acoes){
	$scope.app = "Relatório consolidado dos meus investimentos";

	var rendaFixa = {nome: 'Renda Fixa, ', valorPorAplicacao: [], valorTotal: 0};
	var rendaVariavel = {nome: 'Renda Variável', valorTotal: 0};


	iniciarDadosRendaFixa();
	iniciarDadosRendaVariavel();


	rendaFixa.percentual = rendaFixa.valorTotal / (rendaFixa.valorTotal + rendaVariavel.valorTotal) * 100;
	rendaVariavel.percentual = 100 - rendaFixa.percentual;


	var consolidado = {};
	consolidado.valorTotal = 	rendaFixa.valorTotal + rendaVariavel.valorTotal;
	consolidado.rendaFixa = rendaFixa;
	consolidado.rendaVariavel = rendaVariavel;

	$scope.consolidado = consolidado;
	$scope.mostrarValores = false;

	function iniciarDadosRendaFixa(){
		aplicacoes.data.dados.forEach(function(aplicacao){
			var valor = parseFloat(aplicacao.valor);
			rendaFixa.valorTotal += valor;
			var valorAtualAplicacao = rendaFixa.valorPorAplicacao[aplicacao.idTipoAplicacao] == undefined?
				0 : rendaFixa.valorPorAplicacao[aplicacao.idTipoAplicacao];
			rendaFixa.valorPorAplicacao[aplicacao.idTipoAplicacao] = valorAtualAplicacao + valor;
		});

		rendaFixa.tiposAplicacao = [];
		for (var i = 0; i < rendaFixa.valorPorAplicacao.length ; i++){
			if(rendaFixa.valorPorAplicacao[i] != undefined){
				rendaFixaAPI.getTipoAplicacaoById(i).success(function(data){
					var tipoAplicacao = data.dados[0];
					tipoAplicacao.valor = rendaFixa.valorPorAplicacao[tipoAplicacao.id];
					tipoAplicacao.percentual = tipoAplicacao.valor / rendaFixa.valorTotal * 100;
					rendaFixa.tiposAplicacao.push(tipoAplicacao);
				});
			}
		}
	}

	function iniciarDadosRendaVariavel(){
		acoes.data.dados.forEach(function (acao){
			if(acao.quantidade){
				rendaVariavel.valorTotal += acao.valorTotal -
				(acao.valorTotal / acao.quantidade * (acao.vendidas? acao.vendidas : 0));
			}
		});

		rendaVariavel.acoes = [];
		acoes.data.dados.forEach(function (acao){
			if(acao.quantidade){
				acao.percentual = acao.valorTotal / rendaVariavel.valorTotal * 100;
				rendaVariavel.acoes.push(acao);
			}
		});
	}

	setTimeout(gerarGraficos, 50);

	function gerarGraficos() {
		gerarGraficoTorta('consolidadoGeral', 'Concentração dos investimentos',
			"Tipo investimento", gerarDados([consolidado.rendaVariavel, consolidado.rendaFixa], 'nome', 'percentual'));

		gerarGraficoTorta('consolidadoRendaFixa', 'Concentração dos investimentos de renda fixa',
			"Tipo aplicação",  gerarDados(rendaFixa.tiposAplicacao, 'nome', 'percentual'));

		gerarGraficoTorta('consolidadoRendaVariavel', 'Concentração dos investimentos de renda variável',
				"ação",  gerarDados(rendaVariavel.acoes, 'siglaAcao', 'percentual'));
	}

	function gerarGraficoTorta(divContainer, title, nomeSerie, dados)
	{
		jQuery('#' + divContainer).highcharts({
	        chart: {
	            type: 'pie'
	        },
	        title: {
	            text: title
	        },
					tooltip: {
            pointFormat: 'Porcentual: <b>{point.percentage:.1f}%</b>'
        	},
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: nomeSerie,
	            colorByPoint: true,
	            data: dados
	        }]
	    });
	}

	function gerarDados(array, nameAttr, valueAttr){
		var dados = [];
		array.forEach(function(arrayItem){
			dados.push({
				name: arrayItem[nameAttr],
				y: arrayItem[valueAttr]
			});
		});

		return dados;
	}
});
