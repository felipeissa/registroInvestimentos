angular.module("financeiro").controller("resultadoSimulacaoRendaFixaCtrl", function($scope, simulacao, $modalInstance){
	$scope.app = "Simulação de aplicação de renda fixa";

	var periodo = parseInt(simulacao.periodo);
	var multiplicadorPeriodo = periodo  / 365;

  var tabela = [];
	var valorSimulado = parseFloat(simulacao.valor);
	for(var i = 0; i < simulacao.aplicacoes.length ; i++){
		var aplicacao = simulacao.aplicacoes[i];

		var taxaContratada = 0;
		if(aplicacao.indexador.nome === 'CDI'){
			taxaContratada = getTaxaContratada(simulacao.cdi, aplicacao.taxa);
		}else if(aplicacao.indexador.nome === 'Selic'){
			taxaContratada = getTaxaContratada(simulacao.selic, aplicacao.taxa);
		}else if(aplicacao.indexador.nome === 'Pre fixado'){
			taxaContratada = parseFloat(aplicacao.taxa) / 100;
		}
		var taxaPeriodo = taxaContratada * multiplicadorPeriodo;
		var lucroPeriodo = valorSimulado * taxaPeriodo;
		var lucroBruto = valorSimulado + lucroPeriodo
		var impostoRenda = calcularImpostoRenda(lucroPeriodo, aplicacao.tipoAplicacao, periodo);


		tabela.push({
			nome: aplicacao.tipoAplicacao.nome + ' ' + aplicacao.taxa + "% " + aplicacao.indexador.nome,
			valorBruto: lucroBruto,
			impostoRenda: impostoRenda,
			valorLiquido: lucroBruto - impostoRenda - parseFloat(aplicacao.taxaIntermediacao)
		});
	}

	$scope.resultadoSimulacao = tabela;

	function getTaxaContratada(indexador, taxa){
		return parseFloat(indexador) / 100 * parseFloat(taxa) / 100;
	}

	function calcularImpostoRenda(lucroPeriodo, tipoAplicacao, periodo){
		if(tipoAplicacao.impostoDeRenda){
			var aliquota = 0;
			if(periodo <= 180){
				aliquota = 0.225;
			} else if(periodo <= 365){
			  aliquota = 0.2;
			} else if(periodo <= 720){
				aliquota = 0.175;
			} else {
				aliquota = 0.15;
			}
			return aliquota * lucroPeriodo;
		}
		return 0;
	}
});
