angular.module("financeiro").controller("aplicacaoRendaVariavelCtrl", function($scope, $location, corretoras, acoes, rendaVariavelAPI, $modal, toastr){
	$scope.app = "Nova aplicação de renda variavel";
	$scope.corretoras = corretoras.data.dados;
	$scope.acoes = acoes.data.dados;

	function criarNotaFiscal()
	{
		$scope.notaFiscal = {
		 	data : new Date().getTime(),
		 	negocios : [{corretagem: 0}]
		}
	}
	criarNotaFiscal();

	$scope.adicionarLinha = function()
	{
		$scope.notaFiscal.negocios.push({corretagem: 0});
	}

	$scope.hasLinhaSelecionada = function(linhas)
	{
		return linhas.some(function (linha){
			return linha.selecionado
		});
	}
	$scope.removerLinhasSelecionadas = function(linhas)
	{
		$scope.notaFiscal.negocios = linhas.filter(function (linha){
			if(!linha.selecionado) return linha;
		});
	};

	$scope.confirmarRegistro = function(notaFiscal)
	{
		var modalInstance = $modal.open({
	        animation: false,
	        templateUrl: "view/confirmacaoAplicacaoRendaVariavel.html",
	        controller: "confirmacaoAplicacaoRendaVariavelCtrl",
					size: 'lg',
	        resolve: {
  	            notaFiscal: function () {
	                return notaFiscal;
	            }
	      	}
	    });

	    modalInstance.result.then(function (confirmado) {
	      	if(confirmado){
	      		rendaVariavelAPI.saveNotaFiscal(notaFiscal).success(function(data){
							if(data.status){
								toastr.success('', 'Nota fiscal cadastrada com sucesso!', {timeOut: 2000});
								delete $scope.notaFiscal;
								delete $scope.error;
						 		$scope.notaFiscalForm.$setPristine();
						 		criarNotaFiscal();
							}else{
								toastr.error('', 'Falha ao inserir registro no sistema!', {timeOut: 2000});
								console.log(data.mensagem);
							}
					}).error(function(data){
						toastr.error('', 'Falha ao inserir registro no sistema!', {timeOut: 2000});
						$scope.error = "Não foi possivel salvar os dados.";
						console.log(data.mensagem);
					});
	      }
	    }, function () {
				toastr.warning('', 'Nota fiscal não foi salva!', {timeOut: 2000});
	    });
	}
});
