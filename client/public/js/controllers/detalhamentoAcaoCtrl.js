angular.module("financeiro").controller("detalhamentoAcaoCtrl", function($scope, $modal, acao, compras){
	$scope.app = "Detalhamento Ação";

	$scope.acao = acao;
	$scope.compras = compras.data.dados;

	var quantidadeAcoes = 0;
	var valorTotal = 0;
	var ultimoPrecoMedio = 0;
  compras.data.dados.forEach(function(compra){
		var quantidade = parseInt(compra.quantidade);
		var valorCompra = parseFloat(compra.valor);
		var corretagem = parseFloat(compra.corretagem);

		quantidadeAcoes+= (quantidade * (compra.venda? -1 : 1));
		valorTotal+= ((quantidade * (compra.venda? (ultimoPrecoMedio * -1) : (valorCompra)))
		 + (corretagem * (compra.venda? 0 : 1)));
		compra.acumulado = quantidadeAcoes;
		ultimoPrecoMedio = valorTotal / quantidadeAcoes;
		compra.precoMedio = ultimoPrecoMedio;
	});

	$scope.ultimoPrecoMedio = ultimoPrecoMedio;
});
