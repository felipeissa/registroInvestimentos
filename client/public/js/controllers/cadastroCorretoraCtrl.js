angular.module("financeiro").controller("cadastroCorretoraCtrl", function($scope, $modalInstance, rendaFixaAPI, toastr){
	$scope.app = "Cadastro de corretora"

	$scope.corretora = {};

	$scope.salvarCorretora = function (corretora) {
			corretora.dataCadastro = new Date().getTime();
			rendaFixaAPI.saveCorretora(corretora).success(function(data){
				if(data.status){
					toastr.success('', 'Corretora cadastrada com sucesso!', {timeOut: 2000});
					$modalInstance.close(corretora);
				}else{
					toastr.error('Corretora já cadastrada no sistema!', {timeOut: 2000});
					console.log(data);
				}
			}).error(function(data){
				toastr.error('Falha ao contactar servidor.', {timeOut: 2000});
			});
	};
});
