angular.module("financeiro").controller("listaCorretoraCtrl", function($scope, $modal, corretoras){
	$scope.app = "Corretoras cadastradas";

	$scope.corretoras = corretoras.data.dados;

	$scope.cadastrarCorretora = function()
	{
		var modalInstance = $modal.open({
	        animation: false,
	        templateUrl: "view/cadastroCorretora.html",
	        controller: "cadastroCorretoraCtrl",
					size: 'lg'
	    });

			modalInstance.result.then(function (result) {
				if(result){
					$scope.corretoras.push(result);
				}
			});
	};

	$scope.ordenarPor = function(campo)
	{
		$scope.criterioDeOrdenacao = campo;
		$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	}
});
