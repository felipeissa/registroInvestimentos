angular.module("financeiro").controller("minhaRendaFixaCtrl", function($scope, aplicacoes, rendaFixaAPI){
	$scope.app = "Minhas Aplicações Renda Fixa";

	$scope.aplicacoes = aplicacoes.data.dados;

	$scope.aplicacoes.forEach(function(aplicacao){
		rendaFixaAPI.getCorretoraById(aplicacao.idCorretora).success(function (data){
			aplicacao.corretora = data.dados[0];
		});
		rendaFixaAPI.getTipoAplicacaoById(aplicacao.idTipoAplicacao).success(function (data){
			aplicacao.tipoAplicacao = data.dados[0];
		});
		rendaFixaAPI.getIndexadorById(aplicacao.idIndexador).success(function (data){
			aplicacao.indexador = data.dados[0];
		});
	});



	$scope.ordenarPor = function(campo)
	{
	 	$scope.criterioDeOrdenacao = campo;
	 	$scope.direcaoDaOrdenacao = !$scope.direcaoDaOrdenacao;
	}
});
