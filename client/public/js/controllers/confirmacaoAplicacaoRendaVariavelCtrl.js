angular.module("financeiro").controller("confirmacaoAplicacaoRendaVariavelCtrl", function($scope, notaFiscal, $modalInstance){
	$scope.app = "Confirmação de dados da nota fiscal"

	$scope.notaFiscal = {
		negocios: [],
		liquidacao: notaFiscal.liquidacao,
		iss: notaFiscal.iss,
		corretagem: notaFiscal.corretagem,
		emolumentos: notaFiscal.emolumentos
	};

	var valorTotal = 0;
	notaFiscal.negocios.forEach(function(negocio){
		valorTotal+= (parseFloat(negocio.valor) * parseFloat(negocio.quantidade));
	});

	var liquidacaoNF = parseFloat(notaFiscal.liquidacao);
	var issNF = parseFloat(notaFiscal.iss);
	var emolumentosNF = parseFloat(notaFiscal.emolumentos);
	var corretagemNF = parseFloat(notaFiscal.corretagem);
	var somaGastos = liquidacaoNF + issNF + emolumentosNF;

	notaFiscal.negocios.forEach(function(negocio){
		var valorTotalNegocio = parseFloat(negocio.valor) * parseFloat(negocio.quantidade);
		negocio.corretagem = (notaFiscal.corretagem / notaFiscal.negocios.length) +
		(somaGastos / valorTotal * valorTotalNegocio);
		$scope.notaFiscal.negocios.push({
			acao: negocio.acao,
			valorTotal: valorTotalNegocio,
			corretagem: negocio.corretagem
		});
	});

	valorTotal += liquidacaoNF;
	valorTotal += issNF;
	valorTotal += corretagemNF;
	valorTotal += emolumentosNF;
	$scope.notaFiscal.valorTotal = valorTotal;

	$scope.ok = function () {
	    $modalInstance.close(true);
	};

	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};
});
