angular.module("financeiro").controller("minhaRendaVariavelCtrl", function($scope, $modal, acoes){
	$scope.app = "Minhas Aplicações Renda Variável";

	$scope.acoes = acoes.data.dados;

	$scope.detalhamentoAcao = function(pAcao)
	{
		var modalInstance = $modal.open({
	        animation: false,
	        templateUrl: "view/detalhamentoAcao.html",
	        controller: "detalhamentoAcaoCtrl",
					size: 'lg',
	        resolve: {
  	            acao: function () {
	                return pAcao;
	            	},
								compras: function(rendaVariavelAPI){
				 			 	return rendaVariavelAPI.getConsolidadoAcao(pAcao.idAcao);
				 			 },
	      	}
	    });
	}
});
