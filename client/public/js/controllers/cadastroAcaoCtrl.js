angular.module("financeiro").controller("cadastroAcaoCtrl", function($scope, rendaVariavelAPI, $modalInstance, toastr){
	$scope.app = "Cadastro de ação";

	$scope.salvarAcao = function (acao) {
			acao.dataCadastro = new Date().getTime();
			rendaVariavelAPI.saveAcao(acao).success(function(data){
				if(data.status){
					toastr.success('', 'Ação cadastrada com sucesso!', {timeOut: 2000});
					$modalInstance.close(acao);
				}else{
					toastr.error('Ação já cadastrada no sistema!', {timeOut: 2000});
					console.log(data);
				}
			}).error(function(data){
				toastr.error('Falha ao contactar servidor.', {timeOut: 2000});
			});
	};
});
