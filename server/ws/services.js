//load database queries
var databaseAPI = require('../database/databaseAPI.js');

module.exports = function(app, db) {

  app.get('/tipoAplicacao', isLoggedIn, function(req, res) {
    databaseAPI.getAllDataFromDatabase(['getTipoAplicacao'], [], res, db);
  });

  app.get('/tipoAplicacao/:id', isLoggedIn, function(req, res) {
    var queryArgs = [req.params.id];
    databaseAPI.getAllDataFromDatabase(['getTipoAplicacao', 'whereClause', 'clauseId'],
      queryArgs, res, db);
  });

  app.get('/corretora/', isLoggedIn, function(req, res) {
    var queryArgs = [req.user.id];
    databaseAPI.getAllDataFromDatabase(['getCorretora', 'whereClause', 'clauseUserId'],
      queryArgs, res, db);
  });

  app.get('/corretora/:id', isLoggedIn, function(req, res) {
    var queryArgs = [req.params.id, req.user.id];
    databaseAPI.getAllDataFromDatabase(['getCorretora', 'whereClause', 'clauseId',
    'whereAnd', 'clauseUserId'], queryArgs, res, db);
  });

  app.post('/corretora', isLoggedIn, function(req, res) {
    var queryArgs = [req.body.nome, req.body.dataCadastro,
       req.body.cnpj, req.user.id];
    databaseAPI.getAllDataFromDatabase(['insertCorretora'], queryArgs, res, db);
  });

  app.get('/indexador', isLoggedIn, function(req, res) {
    databaseAPI.getAllDataFromDatabase(['getIndexador'], [], res, db);
  });

  app.get('/indexador/:id', isLoggedIn,function(req, res) {
    var queryArgs = [req.params.id];
    databaseAPI.getAllDataFromDatabase(['getIndexador', 'whereClause', 'clauseId'], queryArgs, res, db);
  });

  app.get('/acao', isLoggedIn,function(req, res) {
    var queryArgs = [req.user.id];
    databaseAPI.getAllDataFromDatabase(['getAcao', 'whereClause',
    'clauseUserId'], queryArgs, res, db);
  });

  app.post('/acao', isLoggedIn,function(req, res) {
    var queryArgs = [req.body.sigla, req.body.nome,
      req.body.dataCadastro, req.user.id];
    databaseAPI.getAllDataFromDatabase(['insertAcao'], queryArgs, res, db);
  });
  app.post('/aplicacaoRendaFixa', isLoggedIn,function(req, res) {
    var queryArgs = [req.body.data, req.body.tipoAplicacao,
      req.body.corretora, req.body.vencimento, req.body.taxa, req.body.indexador,
      req.body.valor, req.user.id]

    databaseAPI.getAllDataFromDatabase(['insertAplicacaoRf'] , queryArgs, res, db);
  });

  app.get('/aplicacaoRendaFixa', isLoggedIn,function(req, res) {
    var queryArgs = [req.user.id];
    databaseAPI.getAllDataFromDatabase(['getAplicacaoRf', 'whereClause',
    'clauseUserId'], queryArgs, res, db);
  });

  app.get('/consolidadoAcoes', isLoggedIn,function(req, res) {
    var queryArgs = [req.user.id];
    databaseAPI.getAllDataFromDatabase(['getConsolidadoRv'], queryArgs, res, db);
  });


  app.get('/consolidadoAcao/:id', isLoggedIn, function(req, res){
      var queryArgs = [req.params.id, db];
      databaseAPI.getAllDataFromDatabase(['getHistoricoAcao'], queryArgs, res, db);
  });

  app.get('/getHistoricoUntilDate/:data', isLoggedIn,function(req, res){
      var queryArgs = [req.params.data];
      databaseAPI.getAllDataFromDatabase(['getHistoricoAcaoIR'], queryArgs, res, db);
  });

  app.post('/notaFiscalRendaVariavel', isLoggedIn,function(req, res) {

    var queryArgs = [req.body.data, req.body.corretora,
      req.body.numero, req.body.liquidacao, req.body.corretagem,
      req.body.emolumentos, req.body.iss, req.user.id];

      var callbackInsertNF = function(err, fields){
        if(err){
          return databaseAPI.generateReturn(res, err);
        }
        var notaId = fields.insertId;
        var negociosArgs = [];
        var acoesUptArgs = [];
        var query = ['insertAplicacaoRvNegocio'];

        var cont = 0;
        req.body.negocios.forEach(function(negocio){
            negociosArgs.push(notaId, negocio.acao.id,
              negocio.quantidade, negocio.valor, negocio.corretagem, negocio.venda);
            if(!negocio.venda){
              acoesUptArgs.push(negocio.acao.id);
            }
            if(cont > 0)
            {
              query.push('insertAplicacaoRvNegocioValues');
            }
            cont+=1;
        });

        var callbackInsertNFAcao = function(err, fields){
          if(err){
            databaseAPI.executeOnDatabase(['deleteAplicacaoRvNegocio', 'whereClause', 'clauseNfId'],
             notaId, _emptyCallbackFunction, db);
            databaseAPI.executeOnDatabase(['deleteAplicacaoRv', 'whereClause', 'clauseId'],
              notaId, _emptyCallbackFunction, db);
          }
          return databaseAPI.generateReturn(res, err);
        }

        databaseAPI.executeOnDatabase(query, negociosArgs, callbackInsertNFAcao, db);

        if(acoesUptArgs.length > 0)
        {
          var callbackUpdateAcoes = function(err, fields){
            if(err){
              console.log('erro atualizando data da compra da ultima acao!!!')
              console.log(err);
            }
          }
          databaseAPI.executeOnDatabase(['updateDtCompraAcao','whereAnd', 'clauseIdIn'],
           [req.body.data, req.body.data, acoesUptArgs], callbackUpdateAcoes, db);
        }
      }

      databaseAPI.executeOnDatabase(['insertAplicacaoRv'],
       queryArgs, callbackInsertNF, db);
  });
}

function _emptyCallbackFunction (err, fields){
  if(err){
    console.log(err);
  }
}

// route middleware to make sure a user is logged in
//TODO: remove, duplicated with route.js
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();
    // if they aren't redirect them to the home page
    res.redirect('/');
}
