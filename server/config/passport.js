// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var crypto = require('crypto');
var bcrypt   = require('bcrypt-nodejs');

//load database API
var databaseAPI = require('../database/databaseAPI.js');

// expose this function to our app using module.exports
module.exports = function(passport, db, serverConfig) {

  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  passport.serializeUser(function(user, done) {
    return done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    var callback = function(err, rows)
    {
      databaseAPI.checkError(err);
      if (!rows) return done(null, false);
      return done(null, rows[0]);
    }
    var data = databaseAPI.executeOnDatabase(['getUser', 'whereClause', 'clauseId'], [id], callback, db);
  });

  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-signup', new LocalStrategy({
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, username, password, done) {
    var callbackSalt = function(err, rows){
      databaseAPI.checkError(err);
      if (rows[0]) {
        return done(null, false, req.flash('signupMessage', 'This username is already taken.'));
      }
      var salt = bcrypt.genSaltSync(5);
      var hash = _hashPassword(password, salt);

      var callbackInsert = function(err, rows){
        databaseAPI.checkError(err);
        var callbackGetNewUser = function(err, rows)
        {
          databaseAPI.checkError(err);
          return done(null, rows[0])
        }
        databaseAPI.executeOnDatabase(['getUser', 'whereClause', 'clauseUsername'], [username], callbackGetNewUser, db);
      }
      databaseAPI.executeOnDatabase(['insertUser'], [username, hash, salt, new Date().getTime()], callbackInsert, db);
    }
    databaseAPI.executeOnDatabase(['getSalt', 'whereClause', 'clauseUsername'],
    [username], callbackSalt, db);
  }
));
// =========================================================================
// LOCAL LOGIN =============================================================
// =========================================================================
// we are using named strategies since we have one for login and one for signup
// by default, if there was no name, it would just be called 'local'

passport.use('local-login', new LocalStrategy({
  // by default, local strategy uses username and password, we will override with email
  usernameField : 'username',
  passwordField : 'password',
  passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, username, password, done) {
  var callBackGetSalt = function(err, rows){
    databaseAPI.checkError(err);
    if (!rows[0]) return done(null, false, req.flash('loginMessage', 'Invalid username'));
    var hash = _hashPassword(password, rows[0].salt);
    var callbackGetNewUser = function(err, rows){
      databaseAPI.checkError(err);
      if (!rows[0]) return done(null, false, req.flash('loginMessage', 'Invalid password'));
      return done(null, rows[0]);
    }
    databaseAPI.executeOnDatabase(['getUser', 'whereClause', 'clauseUsername', 'whereAnd', 'clausePassword'],
    [username, hash], callbackGetNewUser, db);
  }
  databaseAPI.executeOnDatabase(['getSalt', 'whereClause', 'clauseUsername'],
  [username], callBackGetSalt, db);
}));

// =========================================================================
// FACEBOOK ================================================================
// =========================================================================
passport.use(new FacebookStrategy({

  // pull in our app id and secret from our auth.js file
  clientID        : serverConfig.fbClientId,
  clientSecret    : serverConfig.fbClientSecret,
  callbackURL     : serverConfig.fbCallbackUrl

},
// facebook will send back the token and profile
function(token, refreshToken, profile, done) {
  var callBackGetFbUser = function(err, rows){
    databaseAPI.checkError(err);
    if (rows[0]) {
      return done(null, rows[0]);
    }
    var callbackInsertFbUser = function(err, rows) {
      databaseAPI.checkError(err);
      var callbackGetNewUser = function(err, rows){
        databaseAPI.checkError(err);
        return done(null, rows[0]);
      }
      databaseAPI.executeOnDatabase(['getUser', 'whereClause', 'clauseFbId'],
      [profile.id], callbackGetNewUser, db);
    }
    databaseAPI.executeOnDatabase(['insertUserFb'],
    [profile.displayName, (profile.emails? profile.emails[0].value : ''), profile.id, token],
    callbackInsertFbUser, db);
  }
  databaseAPI.executeOnDatabase(['getUser', 'whereClause', 'clauseFbId'],
    [profile.id], callBackGetFbUser, db);
 }

 ));

}

function _hashPassword(password, salt) {
  var hash = crypto.createHash('sha256');
  hash.update(password);
  hash.update(salt);
  return hash.digest('hex');
}
