module.exports = {
    'host'     : process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost',
    'user'     : process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'root',
    'password' : process.env.OPENSHIFT_MYSQL_DB_PASSWORD || 'root',
    'database' : 'controle_investimentos'
};
