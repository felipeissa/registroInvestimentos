#!/bin/env node
//  OpenShift sample Node application
var express = require('express');
var bodyParser = require('body-parser');
var mysql      = require('mysql');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var session      = require('express-session');
var configDB = require(__dirname + '/config/database.js');
var path = require('path');
var databaseAPI = require(__dirname + '/database/databaseAPI.js');

/**
 *  Define the sample application.
 */
var ControleInvestimentosApp = function() {
    //  Scope.
    var self = this;

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;

        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };

    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
    self.terminator = function(sig){
        if (typeof sig === "string") {
           console.log('%s: Received %s - terminating sample app ...',
                       Date(Date.now()), sig);
           process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()) );
    };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
    self.setupTerminationHandlers = function(){
        //  Process on exit and signals.
        process.on('exit', function() { self.terminator(); });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
         'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
            process.on(element, function() { self.terminator(element); });
        });
    };


    /*  ================================================================  */
    /*  App server functions (main app logic here).                       */
    /*  ================================================================  */
    //database configuration
    self.serverConfig = {};
    self.initializeDatabase = function(){
      console.log('connecting to database ' + configDB.database + ' on server '
       + configDB.host + ' with user ' + configDB.user);
      self.db = mysql.createConnection({
        host     : configDB.host,
        user     : configDB.user,
        password : configDB.password,
        database : configDB.database
      });
      console.log('database conection initialized');

      var callbackSysConfig = function(err, rows){
        databaseAPI.checkError(err);
        rows.forEach(function(row){
            self.serverConfig[row.code] = row.value;
        });
        console.log('Server configuration initialized successfully');
        self.initializeServer();
      }
      console.log('initialing server configuration');
      databaseAPI.executeOnDatabase(['getConfiguracaoSistema'], [], callbackSysConfig, self.db);
    };


    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function() {
        self.app = express();

        self.configureServer();
    };

    self.configureServer = function(){
      console.log(self.serverConfig);
      require('./config/passport')(passport, self.db, self.serverConfig); // pass passport for configuration

      self.app.use(morgan('dev')); // log every request to the console
      self.app.use(cookieParser()); // read cookies (needed for auth)
      self.app.use(bodyParser.json()); // get information from html forms
      self.app.use(bodyParser.urlencoded({ extended: true }));

      self.app.set('view engine', 'ejs'); // set up ejs for templating
      self.app.set('views', __dirname + '/views');

      self.app.use(session({ secret: 'myappsecretsoyoucantbreakpasswords' })); // session secret
      self.app.use(passport.initialize());
      self.app.use(passport.session()); // persistent login sessions
      self.app.use(flash()); // use connect-flash for flash messages stored in session
      self.app.use(express.static(__dirname + '/../client/public'));

      // routes ======================================================================
      require('./app/routes.js')(self.app, passport); // load our routes and pass in our app and fully configured passport

      require('./ws/services.js')(self.app, self.db);

      self.start();
    };

    /**
     *  Initializes the sample application.
     */
    self.initialize = function() {
        self.setupVariables();
        self.setupTerminationHandlers();

        //initialize database
        self.initializeDatabase();
    };


    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        //  Start the app on the specific interface (and port).
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...',
                        Date(Date.now() ), self.ipaddress, self.port);
        });
    };

};   /*  Sample Application.  */



/**
 *  main():  Main code.
 */
var zapp = new ControleInvestimentosApp();
zapp.initialize();
