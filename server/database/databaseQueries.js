module.exports = {
    //generic queries
    'whereClause' : ' WHERE ',
    'whereAnd' : ' AND ',
    //where queries
    'clauseId' : ' id = ? ',
    'clauseIdIn' : ' id in (?) ',
    'clauseUsername' : ' username = ? ',
    'clausePassword' : ' password = ? ',
    'clauseFbId' : ' fb_id = ? ',
    'clauseNfId' : ' nota_fiscal_id = ? ',
    'clauseUserId' : ' id_usuario = ? ',
    //login queries
    'getUser' : 'SELECT id, username, fb_id, fb_token, fullname, email FROM USERS ',
    'getSalt' : 'SELECT salt FROM USERS ',

    //inserts
    'insertUser' : 'INSERT INTO USERS (username, password, salt, data_cadastro) values (?,?,?,?)',
    'insertUserFb' : 'INSERT INTO USERS (fullname, email, fb_id, fb_token, data_cadastro) values (?,?,?,?,?)',


    // WS queries
    'getTipoAplicacao' : 'SELECT id, nome, imposto_de_renda impostoDeRenda FROM TIPOS_APLICACAO',
    'getCorretora' : 'SELECT id, nome, data_cadastro as dataCadastro, cnpj from CORRETORA ',
    'getIndexador' : 'SELECT id, nome from INDEXADOR ',
    'getAcao' : 'SELECT id, nome, sigla, data_cadastro dataCadastro, '
        + ' data_ultima_compra dataUltimaCompra FROM ACAO',
    'getAplicacaoRf' : 'select id, data, id_tipo_aplicacao idTipoAplicacao,'
        + ' id_corretora idCorretora, taxa, id_indexador idIndexador, valor, '
        + 'vencimento from APLICACAO_RENDA_FIXA',
    'getConsolidadoRv' : 'SELECT a.sigla siglaAcao, a.id idAcao,'
        + ' a.data_ultima_compra dataUltimaCompra, '
        + '( select sum(n.quantidade) from NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO n '
        + ' where n.id_acao = a.id and n.venda is null) quantidade '
        + ' ,(select sum((n.valor  * n.quantidade) +  n.corretagem) from  '
        + ' NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO n where n.id_acao = a.id and n.venda is null) valorTotal '
        + ' , (select sum (n.quantidade) from  '
        + ' NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO n where n.id_acao = a.id and n.venda = 1) vendidas '
        + ' from ACAO a where id_usuario = ?'
        + ' ORDER BY a.sigla asc',
    'getHistoricoAcao' : 'SELECT n.quantidade, n.valor, n.corretagem, '
        + ' nf.data, n.venda FROM NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO n inner join '
        + ' NOTA_FISCAL_RENDA_VARIAVEL nf on n.id_nota_fiscal = nf.id '
        + ' where n.id_acao = ? ',
    'getHistoricoAcaoIR' : 'SELECT n.quantidade, n.valor, n.corretagem, '
        + ' nf.data, n.venda, n.id_acao, a.sigla, a.nome as nomeAcao, n.id as idTemp '
        + ' FROM NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO n '
        + ' inner join NOTA_FISCAL_RENDA_VARIAVEL nf on n.id_nota_fiscal = nf.id '
        + ' inner join ACAO a on n.id_acao = a.id '
        + ' where nf.data < ? '
        + ' order by a.sigla, nf.data ',

    //WS insert/delete
    'insertCorretora' : 'INSERT INTO CORRETORA (nome, data_cadastro, cnpj,'
        + ' id_usuario) VALUES (?, ?, ?, ?)',
    'insertAcao' : 'INSERT INTO ACAO (sigla, nome, data_cadastro, id_usuario) '
        +' VALUES (?, ?, ?, ?)',
    'insertAplicacaoRf' : 'INSERT INTO APLICACAO_RENDA_FIXA (data, id_tipo_aplicacao,'
        + ' id_corretora, vencimento, taxa, id_indexador, valor, id_usuario) '
        + ' VALUES (?,?,?,?,?,?,?,?)',
    'insertAplicacaoRv' : 'INSERT INTO NOTA_FISCAL_RENDA_VARIAVEL (data, id_corretora,'
        + ' nota_fiscal, liquidacao, emolumentos, corretagem, iss, id_usuario) '
        + ' values (?,?,?,?,?,?,?,?)',
    'insertAplicacaoRvNegocio' : 'INSERT INTO NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO '
        + '(id_nota_fiscal, id_acao, quantidade, valor, corretagem, venda) VALUES (?,?,?,?,?,?)',
    'insertAplicacaoRvNegocioValues' : ',(?,?,?,?,?,?)',
    'deleteAplicacaoRvNegocio' : 'DELETE FROM NOTA_FISCAL_RENDA_VARIAVEL_NEGOCIO',
    'deleteAplicacaoRv' : 'DELETE FROM NOTA_FISCAL_RENDA_VARIAVEL',
    'updateDtCompraAcao' : 'UPDATE ACAO SET DATA_ULTIMA_COMPRA = ? '
        + ' WHERE (DATA_ULTIMA_COMPRA is null or DATA_ULTIMA_COMPRA < ?)',

    //system config
    'getConfiguracaoSistema' : 'select * from CONFIGURACAO_SISTEMA',
};
