var databaseQueries = require(__dirname + '/databaseQueries.js');

module.exports = {
  getQuerie : _getQuery,
  getAllDataFromDatabase : _getAllDataFromDatabase,
  executeOnDatabase : _executeOnDatabase,
  checkError : _checkError,
  generateReturn : _generateReturn
}

function _generateReturn(res, err, data){
  if(err){
    console.log(err);
  }

  var retorno = {
    status: !err,
    mensagem: JSON.stringify(err),
    dados: data
  }

  if(res){
    return res.json(retorno);
  }
  return retorno;
};

function _getQuery(args){
   var querie = '';
   for (var i=0; i<args.length; i++) {
     querie += databaseQueries[args[i]];
   }
   return querie;
 };

 function _getAllDataFromDatabase (queries, queryArgs, res, db){
   query = _getQuery(queries);
     var onQuery = function(err, rows){
       _generateReturn(res, err, rows);
     }
     db.query(query , queryArgs,  onQuery);
 };

 function _executeOnDatabase(queries, queryArgs, callback, db){
   query = _getQuery(queries);
   db.query(query, queryArgs, callback);
 }

 function _checkError(err){
   if(err){
     console.log('Something went wrong!!!!');
     console.log(err);
     return err;
   }
 }
